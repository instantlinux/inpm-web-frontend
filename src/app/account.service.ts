import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

// Backend IP and PORT.
export const SERVER: string = "http://localhost:1337"

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  loggedIn: boolean;
  token: string;
  loginResponse: number;

  // Token content
  username: string;
  id: number;
  loggedInAt: string;
  isAdmin: number;
  isDeveloper: number;

  constructor(private http: HttpClient, private route: Router) { }

  async login(username: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let data: any;
      this.http.post(SERVER + "/auth", {
        username: username,
        password: password
      }, { observe: 'response' }).subscribe((data) => {
        this.loginResponse = data.status;
        console.log("Status: " + data.status);
        

        if (this.loginResponse == 200) {
          const body: any = data.body;
          this.id = body.id;
          this.username = body.username;
          this.isDeveloper = body.isDeveloper;
          this.isAdmin = body.isAdmin;
          this.loggedInAt = body.loggedIn;
          this.loggedIn = true;
          resolve(true);
        }
      }, error => {
        this.loginResponse = 401;
        resolve(false)
      });
    });
  }

  logout() {
    this.token = null;
    this.loggedIn = false;
    this.loggedInAt = null;
    this.username = null;
    this.id = null;
    this.isAdmin = null;
    this.isDeveloper = null;
    this.loginResponse = null;
    this.route.navigate(["/"])
  }
}
