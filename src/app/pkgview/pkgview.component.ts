import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RepositoryService, Package } from '../repository.service';

@Component({
  selector: 'app-pkgview',
  templateUrl: './pkgview.component.html',
  styleUrls: ['./pkgview.component.scss']
})
export class PkgviewComponent implements OnInit {
  pkgRepository: string;

  pkg: Package;

  constructor(private route: ActivatedRoute, private repo: RepositoryService) {
    this.route.params.subscribe(async res => {
      let resp: any = await this.repo.fetchPkg("system", res.pkg);
      this.pkg = resp;

      this.pkgRepository = res.repo;
    });
  }

  ngOnInit() {
  }

  async getDetails(pkgName: string) {
    
  }

}
