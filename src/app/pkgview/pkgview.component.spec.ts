import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PkgviewComponent } from './pkgview.component';

describe('PkgviewComponent', () => {
  let component: PkgviewComponent;
  let fixture: ComponentFixture<PkgviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PkgviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PkgviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
