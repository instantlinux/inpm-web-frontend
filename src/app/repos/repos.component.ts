import { Component, OnInit } from '@angular/core';
import { RepositoryService } from '../repository.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-repos',
  templateUrl: './repos.component.html',
  styleUrls: ['./repos.component.scss']
})
export class ReposComponent implements OnInit {

  constructor(private repo: RepositoryService, private route: Router) { }

  ngOnInit() {
  }

}
