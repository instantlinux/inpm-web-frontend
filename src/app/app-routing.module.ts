import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SearchresultsComponent } from './searchresults/searchresults.component';
import { PkgviewComponent } from './pkgview/pkgview.component';
import { MypkgsComponent } from './mypkgs/mypkgs.component';
import { ReposComponent } from './repos/repos.component';
import { RfepkgsComponent } from './rfepkgs/rfepkgs.component';
import { RepoviewComponent } from './repoview/repoview.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'searchresults/:query',
    component: SearchresultsComponent
  },
  {
    path: 'view/:repo/:pkg',
    component: PkgviewComponent
  },
  {
    path: 'mypkgs',
    component: MypkgsComponent
  },
  {
    path: 'repos',
    component: ReposComponent
  },
  {
    path: 'rfepkgs',
    component: RfepkgsComponent
  },
  {
    path: 'repo/:repo',
    component: RepoviewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
