import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms'
import { AccountService } from '../account.service';
import { Router } from '@angular/router';
import { getHostElement } from '@angular/core/src/render3';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: string;
  pass: string;

  constructor(public account: AccountService, private route: Router) { }

  ngOnInit() {
  }

  async login() {
    if(await this.account.login(this.user, this.pass)) {
      this.route.navigate(["/mypkgs"])
    } else {
      document.getElementsByName("username")[0].style["color"] = "#ff6b6b;";
      document.getElementsByName("username")[0].style["transition"] = "0.4s";
      console.log("Login failed!");
      
    }
    
  }

}
