import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  pkgdownloads = '0';
  pkgviews = '0';
  serverusage = '0GB/0GB';
  mostpopularapps = [];
  mostpopularrfe = [];

  constructor() { }

  ngOnInit() {
    this.mostpopularapps.push('Lorem');
    this.mostpopularapps.push('ipsum');
    this.mostpopularapps.push('dolor');
    this.mostpopularapps.push('sit');
    this.mostpopularapps.push('amet');
    this.mostpopularapps.push('consetetur');
    this.mostpopularapps.push('sadipscing');
    this.mostpopularapps.push('elitr');
    this.mostpopularapps.push('sed');
    this.mostpopularapps.push('diam');
    this.mostpopularapps.push('nonumy');
    this.mostpopularapps.push('eirmod');
    this.mostpopularapps.push('tempor');

    this.mostpopularrfe.push('invidunt');
    this.mostpopularrfe.push('ut');
    this.mostpopularrfe.push('labore');
    this.mostpopularrfe.push('et');
    this.mostpopularrfe.push('dolore');
    this.mostpopularrfe.push('magna');
    this.mostpopularrfe.push('aliquyam');
    this.mostpopularrfe.push('erat');
    this.mostpopularrfe.push('sed');
    this.mostpopularrfe.push('diam');
    this.mostpopularrfe.push('voluptua');
    this.mostpopularrfe.push('At');
  }

}
