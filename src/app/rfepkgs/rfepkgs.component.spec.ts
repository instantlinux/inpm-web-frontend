import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfepkgsComponent } from './rfepkgs.component';

describe('RfepkgsComponent', () => {
  let component: RfepkgsComponent;
  let fixture: ComponentFixture<RfepkgsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfepkgsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfepkgsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
