import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SERVER } from './account.service';

export interface SimplePackage {
  name: string;
  version: string;
  description: string;
  license: string;
  maintainer: string;
  tags: Array<string>;
  downloads: number;
  releasedAt: string;
  updatedAt: string;
  ratings: Array<number>;
}

export interface Package {
  name: string;
  version: string;
  description: string;
  license: string;
  maintainer: string;
  tags: Array<string>;
  dependencies: Array<string>;
  downloads: number;
  views: number;
  releasedAt: string;
  updatedAt: string;
  ratings: Array<Rating>;
  screenshots: Array<Screenshot>;
}

export interface Rating {
  id: number;
  rating: number;
  comment: string;
  edited: boolean;
  editedTimestamp: string;
  timestamp: string;
  user: string;
}

export interface Screenshot {
  id: number;
  index: number;
  type: string;
  title: string;
  description: string;
}

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {

  allRepositorys: Array<String> = ["System"];
  system: Array<SimplePackage>;

  constructor(private http: HttpClient) { }

  async fetch(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      let sysPackages: any = await this.http.get(SERVER + "/system/info").toPromise();
      this.system = sysPackages;
      
      console.log("Fetched system packages: " + JSON.stringify(sysPackages));
      resolve(this.system)
    })    
  }

  async fetchPkg(repository: string, pkg: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let fetched: any;
      fetched = this.http.get(SERVER + "/" + repository + "/" + pkg + "/info").toPromise();
      let res: Package = fetched;
      console.log("Got back: " + JSON.stringify(res));
      
      resolve(res);
    })
  }
}
