import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MypkgsComponent } from './mypkgs/mypkgs.component';
import { PkgviewComponent } from './pkgview/pkgview.component';
import { EditsourceComponent } from './editsource/editsource.component';
import { ReposComponent } from './repos/repos.component';
import { RfepkgsComponent } from './rfepkgs/rfepkgs.component';
import { SearchresultsComponent } from './searchresults/searchresults.component';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Router } from '@angular/router';
import { RepoviewComponent } from './repoview/repoview.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    MypkgsComponent,
    PkgviewComponent,
    EditsourceComponent,
    ReposComponent,
    RfepkgsComponent,
    SearchresultsComponent,
    RepoviewComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [HttpClient/*, {
    provide: HTTP_INTERCEPTORS,
    useFactory: function(injector: Injector, router: Router) {
      return new TokenInterceptor(injector, router);
    },
    multi: true,
    deps: [Router, Injector]
  }*/],
  bootstrap: [AppComponent]
})
export class AppModule { }
