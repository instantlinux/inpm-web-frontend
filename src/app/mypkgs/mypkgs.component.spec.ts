import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MypkgsComponent } from './mypkgs.component';

describe('MypkgsComponent', () => {
  let component: MypkgsComponent;
  let fixture: ComponentFixture<MypkgsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MypkgsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MypkgsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
