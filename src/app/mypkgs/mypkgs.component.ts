import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mypkgs',
  templateUrl: './mypkgs.component.html',
  styleUrls: ['./mypkgs.component.scss']
})
export class MypkgsComponent implements OnInit {

  mypkgs = [];

  constructor() { }

  ngOnInit() {
    for (let i = 0; i < 2; i++) {
      this.mypkgs.push({ icon: '/assets/firefoxicon.png', title: 'Firefox', version: '61.0.1' });
      this.mypkgs.push({ icon: '/assets/unknown.png', title: 'foobar', version: '0.0.5' });
    }
  }

}
