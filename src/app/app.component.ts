import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from './account.service';
import { RepositoryService } from './repository.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  searchText = '';
  title = 'app';

  constructor(private router: Router, private account: AccountService, private repo: RepositoryService) {
    this.ngInit()
  }

  // This code get's executed when a user enter's our website.
  ngInit() {
    console.log("Fetching ...")
    this.repo.fetch()
  }

  searchPkg() {
    this.router.navigateByUrl('/searchresults/' + this.searchText);
  }
}
