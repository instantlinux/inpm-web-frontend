import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RepositoryService, SimplePackage } from '../repository.service';

@Component({
  selector: 'app-repoview',
  templateUrl: './repoview.component.html',
  styleUrls: ['./repoview.component.scss']
})
export class RepoviewComponent implements OnInit {

  repoName: string;
  packages: Array<SimplePackage>;

  constructor(private route: Router, private repo: RepositoryService, private router: ActivatedRoute) {
    this.router.params.subscribe(async res => {
      this.repoName = res.repo;
      await this.repo.fetch();

      // Set package list depends on repository.
      if(this.repoName == "system") {
        this.packages = this.repo.system;
        console.log("System: " + JSON.stringify(this.packages));
      }
    });
  }

  ngOnInit() { }

  navigateTo() {
    this.route.navigate(['/repo', this.repoName, "foobar"])
  }

}
