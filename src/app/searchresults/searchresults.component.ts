import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-searchresults',
  templateUrl: './searchresults.component.html',
  styleUrls: ['./searchresults.component.scss']
})
export class SearchresultsComponent implements OnInit {
  searchquery = '';
  searchresults = [];
  searchresultsrfe = [];

  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(res => this.searchquery = res.query);
    this.searchresults.push({ icon: '/assets/firefoxicon.png', title: 'Firefox' });

    this.searchresultsrfe.push({ icon: '/assets/firefoxicon.png', title: 'Firefox ESR' });
    this.searchresultsrfe.push({ icon: '/assets/firefoxicon.png', title: 'Firefox Beta' });
   }

  ngOnInit() {
  }

}
